DROP DATABASE IF EXISTS M426_ProjektDatabase;
CREATE DATABASE M426_ProjektDatabase;
USE M426_ProjektDatabase;

DROP USER IF EXISTS M426_ProjektDatabase@'%';
CREATE USER M426_ProjektDatabase@'%' IDENTIFIED BY 'M426_ProjektDatabase';
GRANT ALL ON M426_ProjektDatabase.* to 'M426_ProjektDatabase'@'%';
GRANT INSERT, SELECT, UPDATE, DELETE ON m426_ProjektDatabase.* TO M426_ProjektDatabase;



CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO profile (id, username, email, password)
VALUES (1, "ethan", "ethan@gmail.com", "ethanpassword"),
        (2, "Example", "Example@gmail.com", "Examplepassword");
       
INSERT INTO forum (id, username, discussion)
VALUES (1, "ethan", "bookdiscussion"),
			  (2, "example", "examplediscussion");
              
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectname` varchar(100) NOT NULL,
  `projectmembers` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `rating` int(5) NOT NULL, 
  PRIMARY KEY (`id`)
);

INSERT INTO project (id, projectname, projectmembers, description, rating) 
       VALUES (1, "examplename", "exampleprojectname", "Linus war da", 4.5);
