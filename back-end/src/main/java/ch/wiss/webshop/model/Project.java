package ch.wiss.webshop.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Project") // This is from the MySQL Table: "Project"
public class Project {
    
    @Id
    private int Id;
    private String Projectname;
    private String Projectmembers;
    private String description;
    private double rating;

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getProjectname() {
		return Projectname;
	}
	public void setProjectname(String projectname) {
		Projectname = projectname;
	}
	public String getProjectmembers() {
		return Projectmembers;
	}
	public void setProjectmembers(String projectmembers) {
		Projectmembers = projectmembers;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
    
}
