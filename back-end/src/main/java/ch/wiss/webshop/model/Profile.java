package ch.wiss.webshop.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Profile") // This is from the MySQL Table: "Profile"
public class Profile {
	
	// All contents from the table.
	
	@Id
	private int id;
	private String Username;
	private String Email;
	private String Password;
	private String Scrumcoach;
	private String Projektanbieter;
	
	public Profile() {
		
	}
	
	// Getter and Setters
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}

	public String getScrumcoach() {
		return Scrumcoach;
	}

	public void setScrumcoach(String scrumcoach) {
		Scrumcoach = scrumcoach;
	}

	public String getProjektanbieter() {
		return Projektanbieter;
	}

	public void setProjektanbieter(String projektanbieter) {
		Projektanbieter = projektanbieter;
	}
}
