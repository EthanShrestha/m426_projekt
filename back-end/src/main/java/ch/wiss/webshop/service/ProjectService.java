package ch.wiss.webshop.service;

import ch.wiss.webshop.model.Project;

// For saving Projects.

public interface ProjectService {
    public Project saveProject(Project project);
}
