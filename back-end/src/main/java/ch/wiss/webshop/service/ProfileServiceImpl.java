package ch.wiss.webshop.service;

import ch.wiss.webshop.model.Profile;
import ch.wiss.webshop.model.ProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
	
// This is to implement the Profile to ProfileController.

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Override
	public Profile saveProfile(Profile profile) {
		return profileRepository.save(profile);
	}
}
