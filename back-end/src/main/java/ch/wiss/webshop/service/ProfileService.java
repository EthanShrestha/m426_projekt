package ch.wiss.webshop.service;

import ch.wiss.webshop.model.Profile;
// For saving the Profile.

public interface ProfileService {
	public Profile saveProfile(Profile profile);
}
