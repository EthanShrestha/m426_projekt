package ch.wiss.webshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.wiss.webshop.exception.ProjectNotFoundException;
import ch.wiss.webshop.model.Project;
import ch.wiss.webshop.model.ProjectRepository;

@CrossOrigin(origins = "*") // This is to allow access from Back-End to Front-End by allowing Origin (CORS).
@RestController // // This means that this class is a Controller.
@RequestMapping(path = "") // This means URL's start with /project/ (after Application path).
public class ProjectController {
    @Autowired // Injects Project Repository.
    private ProjectRepository projectRepository;

    @GetMapping("/project") // Map only GET Requests with all projects. (Shows all projects)
    public List<Project> getAllProjects() {
        return (List<Project>) projectRepository.findAll();
    }

    @PostMapping("project/add") // Map only POST Requests. (Creates an project)
    Project newProject(@RequestBody Project newProject) {
        return projectRepository.save(newProject);
    }

    @PutMapping("/project/{id}") // To only PUT Requests with a specific project with an id. (Change information)
    Project updateProject(@RequestBody Project newProject, @PathVariable Integer id) {
        return projectRepository.findById(id)
                .map(project -> {
                    project.setProjectname(newProject.getProjectname());
                    project.setProjectmembers(newProject.getProjectmembers());
                    project.setDescription(newProject.getDescription());
                    project.setRating(newProject.getRating());
                    return projectRepository.save(project);
                }).orElseThrow(() -> new ProjectNotFoundException(id));
    }

    @GetMapping("/project/{id}") // Map only GET Requests with a specific project with an id. (Shows specific
                                 // project with an id)
    Project getProjectById(@PathVariable Integer id) {
        return projectRepository.findById(id)
                .orElseThrow(() -> new ProjectNotFoundException(id));
    }

    @DeleteMapping("/project/{id}") // Map only DELETE Requests with a specific project with an id. (Deletes
                                    // specific project with an id)
    String deleteProject(@PathVariable Integer id) {
        if (!projectRepository.existsById(id)) {
            throw new ProjectNotFoundException(id);
        }
        projectRepository.deleteById(id);
        return "User with id " + id + " has been deleted successful";
    }
}
