package ch.wiss.webshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.wiss.webshop.model.Profile; 
import ch.wiss.webshop.model.ProfileRepository;
import ch.wiss.webshop.exception.ProfileNotFoundException;

@CrossOrigin(origins = "*") // This is to allow access from Back-End to Front-End by allowing Origin (CORS).
@RestController // // This means that this class is a Controller.
@RequestMapping(path = "/profile/") // This means URL's start with /profile/ (after Application path).
public class ProfileController {
	
	@Autowired // Injects Profile Repository.
	private ProfileRepository profileRepository;
		
	@GetMapping("/profiles") // Map only GET Requests with all profiles. (Shows all profiles)
	public List<Profile> getAllProfiles() {
		return (List<Profile>) profileRepository.findAll();	
	}
	@PostMapping("/add") // Map only POST Requests. (Creates an profile)
	Profile newProfile(@RequestBody Profile newProfile) {
		return profileRepository.save(newProfile);
	}
	@PutMapping("/profile/{id}") // To only PUT Requests with a specific profile with an id. (Change information)
	Profile updateProfile(@RequestBody Profile newProfile, @PathVariable Integer id) {
		return profileRepository.findById(id)
				.map(profile -> {
					profile.setUsername(newProfile.getUsername());
					profile.setEmail(newProfile.getEmail());
					profile.setPassword(newProfile.getPassword());
					return profileRepository.save(profile);
				}).orElseThrow(() -> new ProfileNotFoundException(id));
	}
	
	@GetMapping("/profile/{id}") // Map only GET Requests with a specific profile with an id. (Shows specific profile with an id)
	Profile getProfileById(@PathVariable Integer id) {
		return profileRepository.findById(id)
				.orElseThrow(() -> new ProfileNotFoundException(id));
	}
	
	@DeleteMapping("/profile/{id}") // Map only DELETE Requests with a specific profile with an id. (Deletes specific profile with an id)
	String deleteProfile(@PathVariable Integer id) {
		if(!profileRepository.existsById(id)) {
			throw new ProfileNotFoundException(id); 
			}
		profileRepository.deleteById(id);
		return "User with id "+id+" has been deleted successful";
		}
}
