package ch.wiss.webshop.exception;

/*
 * 
 * This Exception is thrown when the specific account is not found. 
 * 
 */

public class ProfileNotFoundException extends RuntimeException {
	public ProfileNotFoundException(int id) {
		super("The account with id '" + id + "' could not be found.");
	}
}
