package ch.wiss.webshop.exception;

/*
 * 
 * This Exception is thrown when the specific project is not found. 
 * 
 */

public class ProjectNotFoundException extends RuntimeException {
    public ProjectNotFoundException(int id) {
        super("The project with id '" + id + "' could not be found.");
    }
}
