import './App.css';
import { Routes, Route } from 'react-router-dom';
import Profile from "./components/Layout/Profile/Profile";
import ProjectDetails from './components/Layout/Projects/ProjectDetails';
import Favourites from './components/Layout/Favourites/Favourites';
import Footer from "./components/Layout/Footer/Footer";
import Navbar from "./components/Layout/Navigation/Navbar";
import Searchbar from "./components/Layout/Searchbar/Searchbar";
import Orders from './components/Layout/Orders/Orders';
import NotFound from './components/Layout/NotFound';
import AddProfile from './components/Layout/Profile/AddProfile';
import EditProfile from './components/Layout/Profile/EditProfile';
import ViewProfile from './components/Layout/Profile/ViewProfile';
import Project from './components/Layout/Projects/Project';
import AddProject from './components/Layout/Projects/AddProject';
import EditProject from './components/Layout/Projects/EditProject';
import ViewProject from './components/Layout/Projects/ViewProject';
import Home from './components/Layout/Home/Home';

function App() {

  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} /> 
        <Route path="/project/:id" element={<ProjectDetails />} /> 
        <Route path="/favourites" element={<Favourites />} /> 
        <Route path="/profile" element={<Profile />} />
        <Route path="/searchbar" element={<Searchbar />} />
        <Route path="/orders" element={<Orders />} />
        <Route path="/addprofile" element={<AddProfile />} />
        <Route path="/editprofile/:id" element={<EditProfile />} />
        <Route path="/viewprofile/:id" element={<ViewProfile />} />
        <Route path="/project" element={<Project />} />
        <Route path="/addproject" element={<AddProject />} />
        <Route path="/editproject/:id" element={<EditProject />} />
        <Route path="/viewproject/:id" element={<ViewProject />} />
      <Route path="*" element={<NotFound />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
