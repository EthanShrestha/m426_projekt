import React from 'react';
import '../../../App';

const Home = () => {
  return (
    <div>
          <aside>
		        <h3>Die Seite wo Ihr, Ihre eigene Projekte erstellen könnt</h3>
		        <div className="HomeDashboardWrapp">
			        <div className="box_wrapp">
				        <h2>Super einfach</h2>
				        <p>Erstellen Sie jetzt ganz einfach ihre eigenes <strong>Projekt</strong> nach Ihren eigenen Bedürfnissen und lassen Sie sich von unserem einfachen Benutzeroberfläche beeindrucken.</p>
				    </div>
			        <div className="box_wrapp">
				        <h2>Schönes Design</h2>
				        <p>Unser Motto ist: Design, ist nicht wie es aussieht, sondern wie gut es funktioniert! Da die zuverlässige Applikation sehr schlank gehalten ist, bekommen Sie einen besseren Überblick und Sie können sich voll und ganz auf Ihre Aufgaben konzentrieren! Trotzdem können Sie nach wie vor Ihre Projekte erstellen oder löschen lassen. Durch sein schönes Design und seine kompakten Auftreten lässt es sich leicht bedienen. Bei dem Reiter <strong>Projekte</strong> könnt Ihr ganz einfach Ihre eigenen Projekte erstellen und managen.</p>
				    </div>
				   </div>
			    </aside>

    </div>
  )
}

export default Home;

