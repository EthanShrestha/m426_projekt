import React from 'react';
import '../../../App';
import {Link} from 'react-router-dom';

const Navbar = () => {
  return (
    <div className='navbar'>
        <div>
            <h1>Scrum-Manager</h1>
        </div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/profile">Profil</Link>
            </li>
            <li>
              <Link to="/project">Projekte</Link>
            </li>
            <li>
              <Link to="/favourites">Gemerkte Projekte</Link>
            </li>
            <li>
              <Link to="/orders">Aktive Projekte</Link>
            </li>
            <li>
              <Link to="/searchbar">Verifizierte Projekte</Link>
            </li>
          </ul>
        </nav>
    </div>
  )
}

export default Navbar