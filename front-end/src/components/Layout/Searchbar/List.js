import { React, useState, useEffect } from 'react'
import data from "./ListBook.json"
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

function List(props) {

    const [Project, setProject] = useState([]);

    useEffect(() => {
        loadProject();
    });

    const loadProject = async () => {
        const result = await axios.get("http://localhost:8080/project");
        setProject(result.data);
    }

    const navigate = useNavigate();
    const filteredData = data.filter((el) => {
        if (props.input === '') {
            return el;
        }

        else {
            return el.text.toLowerCase().includes(props.input)
        }
    })

    return (
        <ul>
            {Project.map((project, index) => (
                <li alt="#" onClick={() => navigate(`/project/${project.id}`)} key={project.id}>{project.projectname}</li>
            ))}
            {filteredData.map((item) => (
                <li alt="#" onClick={() => navigate(`/project/${item.id}`)} key={item.id}>{item.text}</li>
            ))}
        </ul>
        
    )
}

export default List
