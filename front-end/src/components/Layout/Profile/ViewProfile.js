import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';

function ViewProfile() {

    const [profile, setProfile] = useState({
        username: "",
        email: "",
        password: ""
    });

    const { id } = useParams();
    
    useEffect(() => {
        loadProfile();
        // eslint-disable-next-line
    }, []);

    const loadProfile = async () => {
        const result = await axios.get(`http://localhost:8080/profile/profile/${id}`)
        setProfile(result.data);
    };


return (
    <div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">User Details</h2>

                <div className="card">
                    <div className="card-header">
                        Details of profile id: {profile.id}
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item">
                                <b>Username:</b>
                                {profile.username}
                            </li>
                            <li className="list-group-item">
                                <b>Email:</b>
                                {profile.email}
                            </li>
                            <li className="list-group-item">
                                <b>Password:</b>
                                {profile.password}
                            </li>
                        </ul>
                    </div>
                </div>
                <Link className="btn btn-primary my-2" to={"/profile"}>Back to Profile</Link>
            </div>
        </div>
    </div>
);
}
export default ViewProfile;