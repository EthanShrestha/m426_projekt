import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Profile";
import { Link, useNavigate, useParams } from "react-router-dom";

export default function EditProfile() {

    let navigate = useNavigate();

    const {id}=useParams()

    const [profile, setProfile] = useState({
        username: "",
        email: "",
        password: ""
    })


    const { username, email, password } = profile

    const onInputChange = (e) => {
        setProfile({ ...profile, [e.target.name]: e.target.value });
    };

    useEffect(() => {
        loadProfile()
        // eslint-disable-next-line
    }, [])

    const onSubmit = async (e) => {
        e.preventDefault();
        await axios.put(`http://localhost:8080/profile/profile/${id}`, profile);
        navigate("/Profile")
    };

    const loadProfile =async ()=>{
        const result = await axios.get(`http://localhost:8080/profile/profile/${id}`)
        setProfile(result.data)
    }

    return (
        <div>
            <button><Link to="/Profile">Back to Profile</Link></button>
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="registration">
                    <h1>Edit User</h1>
                    <label htmlFor="Username">Username</label>
                    <input
                        type={"text"}
                        placeholder="Username..."
                        name="username"
                        value={username}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Email</label>
                    <input
                        type={"text"}
                        placeholder="Email..."
                        name="email"
                        value={email}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Password</label>
                    <input
                        type={"password"}
                        placeholder="Password..."
                        name="password"
                        value={password}
                        onChange={(e) => onInputChange(e)}
                    />
                    <button type="submit"> Register </button>
                </div>
            </form>
        </div>
    )
}
