/* eslint-disable jest/valid-expect */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Profile from './Profile';

/* Mocked Components */
jest.mock("./Profile", () => () => <div>Mocked_Profile</div>);
jest.mock("./AddProfile", () => () => <div>Mocked_AddProfile</div>);
jest.mock("./EditProfile", () => () => <div>Mocked_EditProfile</div>);
jest.mock("./ViewProfile", () => () => <div>Mocked_ViewProfile</div>);

describe("Routing tests for Profile.js", () => {
    test("AddProfiles route should render AddProfile component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/AddProfile"]}>
                <Profile />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Profile"))
    });
    test("EditProfiles route should render EditProfile component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/EditProfile"]}>
                <Profile />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Profile"))
    });
    test("ViewProfiles route should render ViewProfile component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/ViewProfile"]}>
                <Profile />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Profile"))
    });
});