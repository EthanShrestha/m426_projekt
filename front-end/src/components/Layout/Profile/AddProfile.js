import React, { useState } from "react";
import axios from "axios";
import "./Profile";
import { Link, useNavigate } from "react-router-dom";

export default function AddProfile() {

    let navigate = useNavigate();

    const [profile, setProfile] = useState({
        username: "",
        email: "",
        password: ""
    })


    const { username, email, password } = profile

    const onInputChange = (e) => {
        setProfile({ ...profile, [e.target.name]: e.target.value });
    };

    const onSubmit = async (e) => {
        e.preventDefault();
        await axios.post("http://localhost:8080/profile/add", profile);
        navigate("/Profile")
    };

    return (
        <div>
            <button><Link to="/Profile">Back to Profile</Link></button>
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="registration">
                    <h1>Register an profile</h1>
                    <label htmlFor="Username">Username</label>
                    <input
                        type={"text"}
                        required
                        placeholder="Username..."
                        name="username"
                        value={username}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Email</label>
                    <input
                        type={"email"}
                        required
                        placeholder="Email..."
                        name="email"
                        value={email}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Password</label>
                    <input
                        type={"password"}
                        required
                        minLength={8}
                        placeholder="Password..."
                        name="password"
                        value={password}
                        onChange={(e) => onInputChange(e)}
                    />
                    <button type="submit"> Submit </button>
                </div>
            </form>
        </div>
    )
}
