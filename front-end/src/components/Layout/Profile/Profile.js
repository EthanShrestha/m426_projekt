import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../../../App';
import { Link } from 'react-router-dom';

function Profile() {

  const [profile, setProfile] = useState([]);

  useEffect(() => {
    loadProfile();
  });

  const loadProfile = async () => {
    const result = await Axios.get("http://localhost:8080/profile/profiles");
    setProfile(result.data);
  }

  const deleteProfile = async (id) => {
    await Axios.delete(`http://localhost:8080/profile/profile/${id}`)
    loadProfile()
  }

  return (
    <div className="Profile">
      <button><Link to="/AddProfile">Add Profile</Link></button>
      <div>
        <h2 className="text-center">Profile List</h2>
        <div className="py-4">
          <table className="table border shadow">
            <tbody>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Password</th>
                <th scope="col">Action</th>
              </tr>
            </tbody>
            <tbody>
              {profile.map((profile, index) => (
                <tr>
                  <th scope="row">{profile.id}</th>
                  <td>{profile.username}</td>
                  <td>{profile.email}</td>
                  <td>{profile.password}</td>
                  <td>
                    <button className="btn btn-primary xm-2"><Link to={`/ViewProfile/${profile.id}`}>View</Link></button>
                    <button className="btn btn-outline-primary xm-2"> <Link to={`/EditProfile/${profile.id}`}>Edit</Link></button>
                    <button className="btn btn-danger xm-2" onClick={() => deleteProfile(profile.id)}>Delete</button>
                  </td>
                </tr>
              ))};
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Profile