import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import { useAppContext } from '../../context/appContext';

function ViewProject() {

    const [Project, setProject] = useState({
        id: "",
        projectname: "",
        projectmembers: "",
        description: "",
        rating: ""
    });

    const { id } = useParams();

    useEffect(() => {
        loadProject();
        // eslint-disable-next-line
    }, []);

    const loadProject = async () => {
        const result = await axios.get(`http://localhost:8080/project/${id}`)
        setProject(result.data);
    };

    const { favourites, addToFavourites, removeFromFavourites } = useAppContext();

    const { orders, addToOrders, removeFromOrders } = useAppContext();


    const favouritesChecker = (id) => {
        const boolean = favourites.some((project) => project.id === id);
        return boolean;
    };

    const buyerChecker = (id) => {
        const boolean = orders.some((project) => project.id === id);
        return boolean;
    };


    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                    <h2 className="text-center m-4">Project Details</h2>

                    <div className="card">
                        <div className="card-header">
                            Details of Project id: {Project.id}
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item">
                                    <b>Projectname: </b>
                                    {Project.projectname}
                                </li>
                                <li className="list-group-item">
                                    <b>Projectmembers: </b>
                                    {Project.projectmembers}
                                </li>
                                <li className="list-group-item">
                                    <b>Description: </b>
                                    {Project.description}
                                </li>
                                <li className="list-group-item">
                                    <b>Rating: </b>
                                    {Project.rating}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <Link className="btn btn-primary my-2" to={"/Project"}>Back to Project</Link>
                    <div>
                        {favouritesChecker(Project.id) ? (
                            <button onClick={() => removeFromFavourites(Project.id)}>
                                Von Aktiven Projekte entfernen
                            </button>
                        ) : (
                            <button onClick={() => addToFavourites(Project)}>
                                Zu Gemerkte Projekte hinzufügen
                            </button>
                        )};

                        {buyerChecker(Project.id) ? (
                            <button onClick={() => removeFromOrders(Project.id)}>
                                Von Aktiven Projekte entfernen
                            </button>
                        ) : (
                            <button onClick={() => addToOrders(Project)}>
                                Zu Aktiven Projekte hinzufügen
                            </button>
                        )};

                    </div>
                </div>
            </div>
        </div>
    );
}
export default ViewProject;