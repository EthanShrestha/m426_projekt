/* eslint-disable jest/valid-expect */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Project from './Project';

/* Mocked Components */
jest.mock("./Project", () => () => <div>Mocked_Project</div>);
jest.mock("./AddProject", () => () => <div>Mocked_AddProject</div>);
jest.mock("./EditProject", () => () => <div>Mocked_EditProject</div>);
jest.mock("./ViewProject", () => () => <div>Mocked_ViewProject</div>);
jest.mock("./ProjectDetails", () => () => <div>Mocked_ProjectDetails</div>);
jest.mock("./ProjectList", () => () => <div>Mocked_ProjectList</div>);

describe("Routing tests for Project.js", () => {
    test("AddProjects route should render AddProject component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/AddProject"]}>
                <Project />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Project"))
    });
    test("EditProjects route should render EditProjects component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/EditProject"]}>
                <Project />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Project"))
    });
    test("ViewProjects route should render ViewProjects component", () => {
        // Arrange

        // Act
        render(
            <MemoryRouter initialEntries={["/ViewProject"]}>
                <Project />
            </MemoryRouter>
        );

        // Assert
        expect(screen.getByText("Mocked_Project"))
    });
});