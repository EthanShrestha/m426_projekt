import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../../../App';
import { Link } from 'react-router-dom';

function Project() {

    const [Project, setProject] = useState([]);

    useEffect(() => {
        loadProject();
    });

    const loadProject = async () => {
        const result = await Axios.get("http://localhost:8080/project");
        setProject(result.data);
    }

    const deleteProject = async (id) => {
        await Axios.delete(`http://localhost:8080/project/${id}`)
        loadProject()
    }

    return (
        <div className="Project">
            <button><Link to="/AddProject">Add Project</Link></button>
            <div>
                <h2 className="text-center">Project List</h2>
                <div className="py-4">
                    <table className="table border shadow">
                        <tbody>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Projectname</th>
                                <th scope="col">Projectmembers</th>
                                <th scope="col">Description</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tbody>
                        <tbody>
                            {Project.map((project, index) => (
                                <tr>
                                    <th scope="row">{project.id}</th>
                                    <td>{project.projectname}</td>
                                    <td>{project.projectmembers}</td>
                                    <td>{project.description}</td>
                                    <td>{project.rating}</td>
                                    <td>
                                        <button className="btn btn-primary xm-2"><Link to={`/ViewProject/${project.id}`}>View</Link></button>
                                        <button className="btn btn-outline-primary xm-2"> <Link to={`/EditProject/${project.id}`}>Edit</Link></button>
                                        <button className="btn btn-danger xm-2" onClick={() => deleteProject(project.id)}>Delete</button>
                                    </td>
                                </tr>
                            ))};
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Project