import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Project";
import { Link, useNavigate, useParams } from "react-router-dom";

export default function EditProject() {

    let navigate = useNavigate();

    const { id } = useParams()

    const [Project, setProject] = useState({
        projectname: "",
        projectmembers: "",
        description: "",
        rating: ""
    })


    const { projectname, projectmembers, description, rating } = Project

    const onInputChange = (e) => {
        setProject({ ...Project, [e.target.name]: e.target.value });
    };

    useEffect(() => {
        loadProject()
        // eslint-disable-next-line
    }, [])

    const onSubmit = async (e) => {
        e.preventDefault();
        await axios.put(`http://localhost:8080/project/project/${id}`, Project);
        navigate("/Project")
    };

    const loadProject = async () => {
        const result = await axios.get(`http://localhost:8080/project/${id}`)
        setProject(result.data)
    }

    return (
        <div>
            <button><Link to="/Project">Back to Project</Link></button>
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="registration">
                    <h1>Edit Project</h1>
                    <label htmlFor="projectname">Projectname</label>
                    <input
                        type={"text"}
                        placeholder="Projectname..."
                        name="projectname"
                        value={projectname}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Projectmembers</label>
                    <input
                        type={"text"}
                        placeholder="Author..."
                        name="projectmembers"
                        value={projectmembers}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Description</label>
                    <input
                        type={"text"}
                        placeholder="Description..."
                        name="description"
                        value={description}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Rating</label>
                    <input
                        type={"text"}
                        placeholder="Rating..."
                        name="rating"
                        value={rating}
                        onChange={(e) => onInputChange(e)}
                    />
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}
