import React, {useState, useEffect} from 'react';
import '../../../App';
import { API_URL } from '../../../API.js';
import axios from 'axios';
import { useAppContext } from '../../context/appContext';
import {useNavigate} from 'react-router-dom';

const ProjectList = () => {
  const [projects, setProjects] = useState([]);

  const { favourites, addToFavourites, removeFromFavourites } = useAppContext();

  const { orders, addToOrders, removeFromOrders } = useAppContext();

  const navigate = useNavigate();

  const favouritesChecker = (id) => {
    const boolean = favourites.some((project) => project.id === id);
    return boolean;
  };

  const buyerChecker = (id) => {
    const boolean = orders.some((project) => project.id === id);
    return boolean;
  };


  useEffect(() => {
    axios.get(API_URL)
        .then((res) => {
            console.log(res.data);
            setProjects(res.data);
        })
        .catch((err) => console.log(err));
    },  []);

  return (
  <div className='project-list'>
    {projects.map((project) => (
    <div key={project.id} className="project">
            <div>
                    <h4>{project.projectname}</h4>
            </div>
            <div>
          <img src={project.image_url} alt="#" onClick={() => navigate(`http://localhost:8080/project/${project.id}`)}/>
            </div>
                <div>
                    {favouritesChecker(project.id) ? (
                        <button onClick={() => removeFromFavourites(project.id)}>
                            Von Aktiven Projekte entfernen
                        </button>
                    ) : (
                    <button onClick={() => addToFavourites(project)}>
                            Zu Gemerkte Projekte hinzufügen
                        </button>
                    )};

                    {buyerChecker(project.id) ? (
                        <button onClick={() => removeFromOrders(project.id)}>
                            Von Aktiven Projekte entfernen
                        </button>  
                    ) : ( 
                    <button onClick={() => addToOrders(project)}>
                            Zu Aktiven Projekte hinzufügen
                        </button>
                    )};

                </div>
        </div>
        ))}
    </div>
  );
};

export default ProjectList;