import React, { useState } from "react";
import axios from "axios";
import "./Project";
import { Link, useNavigate } from "react-router-dom";

export default function AddProject() {

    let navigate = useNavigate();

    const [project, setProject] = useState({
        id: "",
        projectname: "",
        projectmembers: "",
        description: "",
        rating: ""
    })


    const { id, projectname, projectmembers, description, rating } = project

    const onInputChange = (e) => {
        setProject({ ...project, [e.target.name]: e.target.value });
    };

    const onSubmit = async (e) => {
        e.preventDefault();
        await axios.post("http://localhost:8080/project/add", project);
        navigate("/Project")
    };

    return (
        <div>
            <button><Link to="/Project">Back to Project</Link></button>
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="registration">
                    <h1>Add an Project</h1>
                    <label htmlFor="id">ID</label>
                    <input
                        type={"text"}
                        placeholder="ID..."
                        name="id"
                        value={id}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label htmlFor="Projectname">Projectname</label>
                    <input
                        type={"text"}
                        placeholder="Projectname..."
                        name="projectname"
                        value={projectname}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Projectmembers</label>
                    <input
                        type={"text"}
                        placeholder="Projectmembers..."
                        name="projectmembers"
                        value={projectmembers}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Description</label>
                    <input
                        type={"text"}
                        placeholder="Description..."
                        name="description"
                        value={description}
                        onChange={(e) => onInputChange(e)}
                    />
                    <label>Rating</label>
                    <input
                        type={"text"}
                        placeholder="Rating..."
                        name="rating"
                        value={rating}
                        onChange={(e) => onInputChange(e)}
                    />
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}
