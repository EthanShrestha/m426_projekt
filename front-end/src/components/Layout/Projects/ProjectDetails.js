import React,{useState,useEffect} from 'react';
import "../../../App";
import {useParams } from 'react-router-dom';
import { useAppContext } from '../../context/appContext';
import StarRating from '../StarRating';
import axios from 'axios';

const ProjectDetails = () => {

    const [ project, setProject] = useState({
        id: "",
        projectname: "",
        projectmembers: "",
        description: "",
        rating: ""
    });

    const { id } = useParams();

    useEffect(() => {
        loadProject();
        // eslint-disable-next-line
    }, []);

    const loadProject = async () => {
        const result = await axios.get(`http://localhost:8080/project/${id}`)
        setProject(result.data);
    };

    const { favourites, addToFavourites, removeFromFavourites } = useAppContext();

    const { orders, addToOrders, removeFromOrders } = useAppContext();


    const favouritesChecker = (id) => {
        const boolean = favourites.some((project) => project.id === id);
        return boolean;
    };

    const buyerChecker = (id) => {
        const boolean = orders.some((project) => project.id === id);
        return boolean;
    };

  return (
    <div className="project-details">
        <div className="project-image">
            <h1>{project?.projectname}</h1>
        </div>
        <div className="project-description">
            <h2>Projectmembers</h2>
            <p>{project?.projectmembers}</p>
            <h2>Description</h2>
            <p>{project?.description}</p>
            <h2>Rating</h2>
            <p>{project.rating}</p>
              <StarRating />
              <div>
                  {favouritesChecker(project.id) ? (
                      <button onClick={() => removeFromFavourites(project.id)}>
                          Von Gemerkte Projekte entfernen
                      </button>
                  ) : (
                      <button onClick={() => addToFavourites(project)}>
                          Zu Gemerkte Projekte hinzufügen
                      </button>
                  )};

                  {buyerChecker(project.id) ? (
                      <button onClick={() => removeFromOrders(project.id)}>
                          Von Aktiven Projekte entfernen
                      </button>
                  ) : (
                      <button onClick={() => addToOrders(project)}>
                          Zu Aktiven Projekte hinzufügen
                      </button>
                  )};

              </div>
        </div>
    </div>
  );
}

export default ProjectDetails;