import React from 'react';
import '../../../App';
import { useAppContext } from '../../context/appContext';
import { useNavigate } from 'react-router-dom';

const Favourites = () => {

    const { favourites, addToFavourites, removeFromFavourites } = useAppContext();

    console.log('favourites are', favourites);

    const navigate = useNavigate();

    const favouritesChecker = (id) => {
        const boolean = favourites.some((project) => project.id === id);
        return boolean;
    };


  return (
    <div className="favourites">
          {favourites.length > 0 ? (
            favourites.map((project) => (
              <div key={project.id} className="project">
                  <div>
                  <h4 alt="#" onClick={() => navigate(`/project/${project.id}`)}>{project.projectname}</h4>
                  </div>
                  <div>
                      {favouritesChecker(project.id) ? (
                          <button onClick={() => removeFromFavourites(project.id)}>
                              Von Gemerkte Projekte entfernen
                          </button>
                      ) : (
                          <button onClick={() => addToFavourites(project)}>
                              Zu Gemerkte Projekte hinzufügen
                          </button>
                        )}
                  </div>
              </div>
            ))
        )  :  ( 
          <h1>Du hast keine gemerkte Projekte</h1>
        )}
      </div>
    );      
};
export default Favourites;