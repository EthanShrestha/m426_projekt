import React from 'react';
import '../../../App';
import { useAppContext } from '../../context/appContext';
import { useNavigate } from 'react-router-dom';

const Orders = () => {

    const { orders, addToOrders, removeFromOrders } = useAppContext();

    console.log('orders are', orders);

    const navigate = useNavigate();

    const ordersChecker = (id) => {
        const boolean = orders.some((project) => project.id === id);
        return boolean;
    };

    return (
        <div className="favourites">
            {orders.length > 0 ? (
                orders.map((project) => (
                    <div key={project.id} className="project">
                        <div>
                        <h4 alt="#" onClick={() => navigate(`/project/${project.id}`)}>{project.projectname}</h4>
                        </div>
                        <div>
                            {ordersChecker(project.id) ? (
                                <button onClick={() => removeFromOrders(project.id)}>
                                    Von Aktiven Projekte entfernen
                                </button>
                            ) : (
                                <button onClick={() => addToOrders(project)}>
                                    Zu Aktiven Projekte hinzufügen
                                </button>
                            )}
                        </div>
                    </div>
                ))
            ) : (
                <h1>Du hast keine aktive Projekte</h1>
            )}
        </div>
    );
};
export default Orders;