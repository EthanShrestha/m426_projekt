/* eslint-disable jest/valid-expect */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import App from './App';

/* Mocked Components */
jest.mock("./", () => () => <div>Mocked_Home</div>);
jest.mock("./components/Layout/Profile/Profile", () => () => <div>Mocked_Profile</div>);
jest.mock("./components/Layout/Projects/Project", () => () => <div>Mocked_Project</div>);
jest.mock("./components/Layout/Projects/AddProject", () => () => <div>Mocked_AddProject</div>);
jest.mock("./components/Layout/Projects/EditProject", () => () => <div>Mocked_EditProject</div>);
jest.mock("./components/Layout/Projects/ViewProject", () => () => <div>Mocked_ViewProject</div>);
jest.mock("./components/Layout/Favourites/Favourites", () => () => <div>Mocked_Favourites</div>);
jest.mock("./components/Layout/Footer/Footer", () => () => <div>Mocked_Footer</div>);
jest.mock("./components/Layout/Navigation/Navbar", () => () => <div>Mocked_Navbar</div>);
jest.mock("./components/Layout/Searchbar/Searchbar", () => () => <div>Mocked_Searchbar</div>);
jest.mock("./components/Layout/Orders/Orders", () => () => <div>Mocked_Orders</div>);
jest.mock("./components/Layout/Profile/AddProfile", () => () => <div>Mocked_AddProfile</div>);
jest.mock("./components/Layout/Profile/EditProfile", () => () => <div>Mocked_EditProfile</div>);
jest.mock("./components/Layout/Profile/ViewProfile", () => () => <div>Mocked_ViewProfile</div>);
jest.mock("./components/Layout/NotFound", () => () => <div>Mocked_NotFound</div>);

describe("Routing test for App.js", () => {
  test("Profiles route should render Profile component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/Profile"]}>
        <App/>
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_Profile"))
    expect(screen.getByText("Mocked_Navbar"))
  });

  test("Projects route should render Project component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/Project"]}>
        <App />
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_Project"))
    expect(screen.getByText("Mocked_Navbar"))
  });

  test("Favourites route should render Favourite component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/Favourites"]}>
        <App />
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_Favourites"))
    expect(screen.getByText("Mocked_Navbar"))
  });

  test("Orders route should render Orders component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/Orders"]}>
        <App />
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_Orders"))
    expect(screen.getByText("Mocked_Navbar"))
  });

  test("Searchbars route should render Searchbar component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/Searchbar"]}>
        <App />
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_Searchbar"))
    expect(screen.getByText("Mocked_Navbar"))
  });

  test("Unknown path should trigger NotFound component", () => {
    // Arrange

    // Act
    render(
      <MemoryRouter initialEntries={["/NotFound"]}>
        <App/>
      </MemoryRouter>
    );

    // Assert
    expect(screen.getByText("Mocked_NotFound"))
  });
  
});